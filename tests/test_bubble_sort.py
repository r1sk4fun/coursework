from srs.lib import bubble_sort
from srs.util import list_tuples, create_dict


not_sorted_list = list_tuples()
sorted_list = sorted(not_sorted_list)
sorted_list_reversed = sorted(not_sorted_list, reverse=True)


def test_ascending_order():
    result = bubble_sort(not_sorted_list, reverse=False)
    assert result == sorted_list


def test_descending_order():
    result = bubble_sort(not_sorted_list, reverse=True)
    assert result == sorted_list_reversed


def test_stable_ascending():
    result = bubble_sort(not_sorted_list, reverse=False)
    assert create_dict(result) == create_dict(sorted_list)


def test_stable_descending():
    result = bubble_sort(not_sorted_list, reverse=True)
    assert create_dict(result) == create_dict(sorted_list_reversed)
