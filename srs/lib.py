import srs.util


def counter():
    return None


def bubble_sort(arr, reverse=False):
    n = len(arr)
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            counter()
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    if reverse:
        return arr[::-1]
    else:
        return arr


def shell_sort(arr, reverse=False):
    n = len(arr)
    gap = n // 2
    while gap > 0:
        for i in range(gap, n):
            counter()
            temp = arr[i]
            j = i
            while j >= gap and arr[j - gap] > temp:
                arr[j] = arr[j - gap]
                j -= gap
            arr[j] = temp
        gap //= 2
    if reverse:
        return arr[::-1]
    else:
        return arr


def selection_sort(arr, reverse=False):
    n = len(arr)
    for i in range(n):
        minimum = i
        for j in range(i + 1, n):
            counter()
            if arr[j] < arr[minimum]:
                minimum = j
        arr[minimum], arr[i] = arr[i], arr[minimum]
    if reverse:
        return arr[::-1]
    else:
        return arr


def insertion_sort(arr, left=0, right=None):
    if right is None:
        right = len(arr) - 1
    for i in range(left + 1, right + 1):
        counter()
        key_item = arr[i]
        j = i - 1
        while j >= left and arr[j] > key_item:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key_item
    return arr


def timsort(arr, reverse=False):
    min_run = 32
    n = len(arr)
    for i in range(0, n, min_run):
        insertion_sort(arr, i, min((i + min_run - 1), n - 1))
    size = min_run
    while size < n:
        for start in range(0, n, size * 2):
            counter()
            midpoint = start + size - 1
            end = min((start + size * 2 - 1), (n-1))
            merged_array = srs.util.merge(
                left=arr[start:midpoint + 1],
                right=arr[midpoint + 1:end + 1])
            arr[start:start + len(merged_array)] = merged_array
            size *= 2
    if reverse:
        return arr[::-1]
    else:
        return arr
