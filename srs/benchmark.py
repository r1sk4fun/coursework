from matplotlib import pyplot as plt
from random import randint
import cProfile
import pstats
import srs.lib


def create_array(length):
    array = [(randint(1, 10), randint(1, 10)) for i in range(1, length + 1)]
    return array


def line_with_calls(file):
    result = 1
    lines = [i.strip() for i in file]
    lines_1 = [i for i in lines if i not in '' and i.split()[0].isnumeric() and not i.count('function')]
    for line in lines_1:
        if line.find('counter') != -1:
            result = line.split('   ')[0]
            break
    return result


def graph(x_lst, y_lst):
    plt.plot(x_lst, y_lst, '-b', label='Эксперем. сложность')
    plt.xlabel('Размер массива')
    plt.ylabel('Количество операций')
    plt.legend()
    plt.tight_layout()
    plt.show()


first = 6 * 2 * 2
last = 6 * 20 * 2
step = 6 * 2 * 2

array_len = [i for i in range(first, last, step)]
operations = []

func = ['srs.lib.bubble_sort(array)',
        'srs.lib.shell_sort(array)',
        'srs.lib.selection_sort(array)',
        'srs.lib.timsort(array)']

for e in func:
    for i in range(first, last, step):
        array = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = line_with_calls(f)
        f.close()
        operations.append(int(line))
    graph(array_len, operations)
    operations = []
