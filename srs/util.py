def check_input(data):
    data = data.replace(' ', '')
    if data.isdigit():
        return True
    else:
        return False


def list_tuples():
    new_list = [(7, 1), (1, 1), (4, 3), (5, 10), (3, 7), (2, 5), (6, 8), (9, 12), (8, 1), (10, 4)]
    return new_list


def create_dict(arr):
    result_dict = {k[0]: [] for k in arr}
    for index, num in arr:
        result_dict[index].append(num)
    return result_dict


def merge(left, right):
    if len(left) == 0:
        return right
    if len(right) == 0:
        return left
    result = []
    index_left = index_right = 0
    while len(result) < len(left) + len(right):
        if left[index_left] <= right[index_right]:
            result.append(left[index_left])
            index_left += 1
        else:
            result.append(right[index_right])
            index_right += 1
        if index_right == len(right):
            result += left[index_left:]
            break
        if index_left == len(left):
            result += right[index_right:]
            break
    return result
