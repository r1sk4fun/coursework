import srs.lib
import srs.util


flag = True
while flag:
    array = input('Введите элементы массива для сортировки через пробел: ').split(' ')
    print('Пузырьковая - 1; Шелла - 2; Выбором - 3; Timsort - 4')
    choice_1 = input('Введите номера сортировок без пробелов: ')
    choice_2 = input('Сортировка по возрастанию - 1, сортировка по убыванию - 2: ')

    if choice_2 == '1':
        rev_flag = False
    elif choice_2 == '2':
        rev_flag = True
    else:
        print('Ошибка входных данных')
        continue

    if srs.util.check_input(choice_1) is True:
        for e in choice_1:
            if e == '1':
                result = srs.lib.bubble_sort(array, reverse=rev_flag)
                print(f'Пузырьковая сортировка: \n{result}')
            elif e == '2':
                result = srs.lib.shell_sort(array, reverse=rev_flag)
                print(f'Сортировка Шелла: \n{result}')
            elif e == '3':
                result = srs.lib.selection_sort(array, reverse=rev_flag)
                print(f'Сортировка выбором: \n{result}')
            elif e == '4':
                result = srs.lib.timsort(array, reverse=rev_flag)
                print(f'Timsort: \n{result}')
            else:
                print('Ошибка входных данных')
    else:
        print('Ошибка входных данных')
        continue

    print('Хотите продолжить? (Y/n)')
    if input() not in ['Y', 'y']:
        flag = False
    else:
        pass
